package th.ac.tu.siit.lab9googlemaps;

import java.util.Random;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends FragmentActivity implements
		ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

	GoogleMap map;
	Location currentLocation;
	LocationClient locClient;
	LocationRequest locRequest;
	LocationManager locManager;
	int color = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		MapFragment mapFragment = (MapFragment) getFragmentManager()
				.findFragmentById(R.id.map);
		map = mapFragment.getMap();
		map.setMyLocationEnabled(true);
		// map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		locClient = new LocationClient(this, this, this);

		/*
		 * MarkerOptions m = new MarkerOptions(); m.position(new LatLng(0, 0));
		 * map.addMarker(m);
		 * 
		 * PolylineOptions p = new PolylineOptions(); p.add(new LatLng(0, 0));
		 * p.add(new LatLng(13.75, 100.4667)); p.width(10); p.color(Color.BLUE);
		 * map.addPolyline(p);
		 */

		/*
		 * locManager =
		 * (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		 * currentLocation =
		 * locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		 * LocationListener locListener = new LocationListener() {
		 * 
		 * @Override public void onProviderDisabled(String provider) { }
		 * 
		 * @Override public void onProviderEnabled(String provider) { }
		 * 
		 * @Override public void onStatusChanged(String provider, int status,
		 * Bundle extras) { } };
		 * locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000,
		 * 5, locListener);
		 */
	}

	protected void onStart() {
		super.onStart();
		locClient.connect();
	}

	protected void onStop() {
		locClient.disconnect();
		super.onStop();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {

	}

	@Override
	public void onConnected(Bundle arg0) {
		currentLocation = locClient.getLastLocation();

		locRequest = LocationRequest.create();
		locRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		locRequest.setInterval(5 * 1000);
		locRequest.setFastestInterval(1 * 1000);
		locClient.requestLocationUpdates(locRequest, this);
	}

	@Override
	public void onDisconnected() {

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		switch (id) {
		case R.id.mark:
			MarkerOptions mo = new MarkerOptions();
			mo.position(new LatLng(currentLocation.getLatitude(),
					currentLocation.getLongitude()));
			mo.title("Mark");
			map.addMarker(mo);
			break;

		case R.id.color:

			color *= -1;

		}

		return super.onOptionsItemSelected(item);

	}

	@Override
	public void onLocationChanged(Location l) {
		/*
		 * if (currentLocation.distanceTo(l) > 5) { currentLocation = l;
		 * 
		 * MarkerOptions m = new MarkerOptions(); m.position(new
		 * LatLng(l.getLatitude(), l.getLongitude())); map.addMarker(m);
		 */

		PolylineOptions po = new PolylineOptions();
		po.add(new LatLng(currentLocation.getLatitude(), currentLocation
				.getLongitude()));
		po.add(new LatLng(l.getLatitude(), l.getLongitude()));
		po.width(10);
		po.color(Color.GREEN);
		currentLocation = l;
		if (color == 1)

		{
			po.color(Color.GREEN);

		} else {
			po.color(Color.RED);
		}
		map.addPolyline(po);

		currentLocation = l;
		po.width(10);

	}

}
